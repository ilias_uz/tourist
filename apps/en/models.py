from __future__ import unicode_literals
from redactor.fields import RedactorField
from django.db import models

# Create your models here.


class Country(models.Model):
    country_name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.country_name


class CountryInfo(models.Model):
    choices_country = models.ForeignKey(Country, related_name='en_country')
    title = models.CharField(max_length=500)
    body = RedactorField(verbose_name=u'Text', allow_file_upload=False, allow_image_upload=False)

    def __unicode__(self):
        return self.title


class MainInfo(models.Model):
    title = models.CharField(max_length=400)
    body = RedactorField(verbose_name=u'Text', allow_file_upload=False, allow_image_upload=False)

    def __unicode__(self):
        return self.title


class LeftBar(models.Model):
    title = models.CharField(max_length=400)
    body = RedactorField(verbose_name=u'Text', allow_file_upload=False, allow_image_upload=False)
    image = models.ImageField(upload_to='media', blank=True)
    out_href = models.CharField(max_length=1000, help_text='www.google.com, www.yandex.ru', blank=True)
    out_link = models.BooleanField()
    link = models.BooleanField()
    not_commercial = models.BooleanField()

    def __unicode__(self):
        return self.title


class Commercial(models.Model):
    title = models.CharField(max_length=400)
    url = models.URLField()
    image = models.ImageField(upload_to='media')

    def __unicode__(self):
        return self.title


class Information(models.Model):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='dop_info_image')
    body = RedactorField(verbose_name=u'Text', allow_file_upload=False, allow_image_upload=False)

    def __unicode__(self):
        return self.title