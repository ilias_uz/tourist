"""tourist URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^$', 'apps.en.views.index', name='index_ru'),
    url(r'^info/(?P<id>[0-9]+)/', 'apps.en.views.country_info', name='country_info_en'),
    url(r'^information/(?P<id>[0-9]+)/', 'apps.en.views.information', name='information_en'),
    url(r'^left_bar/(?P<id>[0-9]+)/', 'apps.en.views.left_bar', name='left_bar_en'),
]
