from django.contrib import admin
from models import CountryInfo, Country, MainInfo, LeftBar, Commercial, Information
# Register your models here.

admin.site.register(CountryInfo)
admin.site.register(Country)
admin.site.register(MainInfo)
admin.site.register(LeftBar)
admin.site.register(Commercial)
admin.site.register(Information)