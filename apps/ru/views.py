from django.shortcuts import render
from models import MainInfo, Country, CountryInfo, LeftBar, Commercial, Information
# Create your views here.


def index(request):
    main_info = MainInfo.objects.all()[:1]
    country = Country.objects.all()
    left_bar = LeftBar.objects.all()
    commercial = Commercial.objects.all()
    information = Information.objects.all()
    return render(request, 'ru/index.html', {'main_info': main_info, 'country': country, 'left_bar': left_bar,
                                             'information': information, 'commercial': commercial})


def country_info(request, id):
    country_info = CountryInfo.objects.get(choices_country=id)
    country = Country.objects.all()
    left_bar = LeftBar.objects.all()
    commercial = Commercial.objects.all()
    return render(request, 'ru/country_info.html', {'country_info': country_info, 'country': country,
                                                    'left_bar': left_bar, 'commercial': commercial})


def left_bar(request, id):
    country = Country.objects.all()
    left_bar = LeftBar.objects.all()
    left_bar_info = LeftBar.objects.get(pk=id)
    commercial = Commercial.objects.all()
    return render(request, 'ru/left_bar.html', {'left_bar_info': left_bar_info, 'country': country,
                                                'left_bar': left_bar, 'commercial': commercial})


def information(request, id):
    country = Country.objects.all()
    left_bar = LeftBar.objects.all()
    information = Information.objects.get(pk=id)
    commercial = Commercial.objects.all()
    return render(request, 'ru/dop_info.html', {'left_bar_info': information, 'country': country,
                                                'left_bar': left_bar, 'commercial': commercial})